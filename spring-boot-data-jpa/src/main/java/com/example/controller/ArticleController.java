package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.controller.dto.ArticleDto;
import com.example.service.ArticleService;
/**
 * 
 * @author Kumar Panchal
 *
 */
@RestController
@RequestMapping("/article")
public class ArticleController {

	@Autowired
	private ArticleService articleService;
	
	@GetMapping()	
	public ArticleDto get(@RequestParam final String articleName) {
		return articleService.get(articleName);
	}
	
	@PostMapping
	public void save(@RequestBody final ArticleDto article) {
		articleService.save(article);
	}
}