package com.example.controller.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Kumar Panchal
 *
 */
@Setter
@Getter
public class ArticleDto {

	private String articleName;

	private String publisherName;

	public ArticleDto(String articleName, String publisherName) {
		super();
		this.articleName = articleName;
		this.publisherName = publisherName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((articleName == null) ? 0 : articleName.hashCode());
		result = prime * result + ((publisherName == null) ? 0 : publisherName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArticleDto other = (ArticleDto) obj;
		if (articleName == null) {
			if (other.articleName != null)
				return false;
		} else if (!articleName.equals(other.articleName))
			return false;
		if (publisherName == null) {
			if (other.publisherName != null)
				return false;
		} else if (!publisherName.equals(other.publisherName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ArticleDto [articleName=" + articleName + ", publisherName=" + publisherName + "]";
	}
}
