package com.example.convertor;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.controller.dto.ArticleDto;
import com.example.model.Article;

/**
 * 
 * @author Kumar Panchal
 *
 */
@Component
public class ArticleConvertor implements Converter<ArticleDto, Article> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Article convert(final ArticleDto articleDto) {
		final Article article = new Article(articleDto.getArticleName(), articleDto.getPublisherName());
		return article;
	}

}
