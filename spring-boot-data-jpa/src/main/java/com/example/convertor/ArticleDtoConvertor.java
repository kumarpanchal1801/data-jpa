package com.example.convertor;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.controller.dto.ArticleDto;
import com.example.model.Article;
/**
 * 
 * @author Kumar Panchal
 *
 */
@Component
public class ArticleDtoConvertor implements Converter<Article, ArticleDto> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArticleDto convert(Article article) {
		final ArticleDto articleDto = new ArticleDto(article.getArticleName(),article.getPublisherName());
		return articleDto;
	}

}
