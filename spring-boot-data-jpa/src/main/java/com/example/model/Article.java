package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
/**
 * 
 * @author Kumar Panchal
 *
 */
@Entity(name="article")
@Setter
@Getter
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="article_name")
	private String articleName;

	@Column(name="publisher_name")
	private String publisherName;

	public Article() {}
	
	public Article(String articleName, String publisherName) {
		super();
		this.articleName = articleName;
		this.publisherName = publisherName;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", articleName=" + articleName + ", publisherName=" + publisherName + "]";
	}
}
