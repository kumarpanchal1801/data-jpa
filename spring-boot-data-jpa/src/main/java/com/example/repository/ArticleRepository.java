package com.example.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.model.Article;
/**
 * 
 * @author Kumar Panchal
 *
 */
@Repository
public interface ArticleRepository extends CrudRepository<Article, Long> {

	/**
	 * Method to fetch data from article name
	 * 
	 * @param articleName
	 * @return
	 */
	@Query("SELECT a from article a where a.articleName= :articleName")
	public Article findByArticleName(@Param("articleName") String articleName);
	
}
