package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import com.example.controller.dto.ArticleDto;
import com.example.model.Article;
import com.example.repository.ArticleRepository;
/**
 * 
 * @author Kumar Panchal
 *
 */
@Service
public class ArticleService {

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private ConversionService conversionService;

	/**
	 * This method will persist the data in {@link Article}
	 * 
	 * @param articleDto
	 */
	public void save(ArticleDto articleDto) {
		final Article article = conversionService.convert(articleDto, Article.class);
		articleRepository.save(article);
	}

	/**
	 * This method will process the fetch data using articleName
	 * 
	 * @param articleName
	 * @return
	 */
	public ArticleDto get(final String articleName) {
		final Article article = articleRepository.findByArticleName(articleName);
		final ArticleDto articleDto = conversionService.convert(article, ArticleDto.class);
		return articleDto;
	}
}