DROP TABLE IF EXISTS article;
 
CREATE TABLE article (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  article_name VARCHAR(250) NOT NULL,
  publisher_name VARCHAR(250) NOT NULL
);
 
INSERT INTO article (article_name, publisher_name) VALUES
  ('Flood in vadodara', 'Gujarat Samachar'),
  ('Kashmir 370', 'Times of india'),
  ('Weather forecase', 'Times of india');