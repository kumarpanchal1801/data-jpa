package com.example.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.example.controller.dto.ArticleDto;
import com.example.service.ArticleService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(ArticleController.class)
public class ArticleControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	
	@MockBean
	private ArticleService articleService;
	
	@Test
	public void testGet() throws Exception {
		ArticleDto articleDto = new ArticleDto("test", "test");
		
		when(articleService.get("test")).thenReturn(articleDto);
		
		mockMvc.perform(get("/article").param("articleName", "test")).andExpect(status().isOk());
	}
	
	@Test
	public void testSave() throws Exception {
		ArticleDto articleDto = new ArticleDto("test", "test");
		 mockMvc.perform(post("/article").contentType(MediaType.APPLICATION_JSON).content(asJsonString(articleDto))).andExpect(status().isOk());
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
}
