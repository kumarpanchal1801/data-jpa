package com.example.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.model.Article;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ArticleRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private ArticleRepository articleRepository;
	
	@Test
	public void testGet() {
		final Article article = new Article("Test", "Test");
		entityManager.persist(article);
		entityManager.flush();
		final Article _article = articleRepository.findByArticleName("Test");
		assertThat(_article.getArticleName()).isEqualTo(article.getArticleName());
	}
	
	@Test
	public void testSave() {
		final Article article = new Article("Test", "Test");
		
		final Article _article = articleRepository.save(article);
		
		assertThat(_article.getArticleName()).isEqualTo(article.getArticleName());
	}
}
