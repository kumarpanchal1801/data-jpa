package com.example.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.SpringBootDataJpaApplicationTests;
import com.example.controller.dto.ArticleDto;
import com.example.model.Article;
import com.example.repository.ArticleRepository;

@RunWith(SpringRunner.class)
public class ArticleServiceTest extends SpringBootDataJpaApplicationTests{

	@Autowired
	private ArticleService articleService;
	
	@MockBean
	private ArticleRepository articleRepository;
	
	
	@Test
	public void testSave() {
		final ArticleDto articleDto = new ArticleDto("test", "test");
		
		articleService.save(articleDto);
	}
	
	@Test
	public void testGet() {
		final Article article = new Article("test", "test");
		when(articleRepository.findByArticleName("test")).thenReturn(article);
		final ArticleDto articleDto = articleService.get("test");
		assertThat(articleDto.getArticleName()).isEqualTo(article.getArticleName());
	}
}
